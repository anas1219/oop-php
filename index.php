<?php


    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $animal = new animal("shaun");
    echo "Name = " .$animal->name ."<br>"; 
    echo "Legs = ".$animal->legs ."<br>"; 
    echo "Cold blooded = " .$animal->cold_blooded ." <br> <br>" ; 


    $frog = new frog("buduk");
    echo "Name = " .$frog->name ."<br>"; 
    echo "Legs = ".$frog->legs ."<br>"; 
    echo "Cold blooded = " .$frog->cold_blooded ." <br>" ; 
    echo $frog  -> jump() ." <br> <br>" ; 


    $ape = new ape("kera sakti");
    echo "Name = " .$ape->name ."<br>"; 
    echo "Legs = ".$ape->legs ."<br>"; 
    echo "Cold blooded = " .$ape->cold_blooded ." <br>" ; 
    echo $ape  -> yell()  ; 
?>